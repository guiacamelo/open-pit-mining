#include <fstream>
#include <iostream>
#include <stdlib.h>
#include "graph.h"
#include <cassert>

using namespace std;


Graph::Graph()
{
}

vector<int> Graph::loadGraph(string fileName)
{
	ifstream fs;
	string in;
	vector<int> allEdges(0);
	int origin, target, capacity,flow;
	for(int i = 0; i < graph.size(); i++) graph.pop_back();
		for(int i = 0; i < allEdges.size(); i++) allEdges.pop_back();
			fs.open(fileName.c_str());
		if (fs.is_open()){
			while(!fs.eof()){
				fs >> in;
				if(!in.compare("a")){
					fs>>in;origin = atoi (in.c_str());
					fs>>in;target = atoi (in.c_str());
					fs>>in;capacity = atoi (in.c_str());
					flow = 0;
				addEdge(origin,target,capacity,flow, FORWARD_EDGE); // isForward = true 
				allEdges.push_back(origin);allEdges.push_back(target);allEdges.push_back(capacity);
			} else if (!in.compare("p")) {
				string dummy;
				unsigned nV, QtdEdges;
				fs >> dummy >> nV >> QtdEdges;
				graph.resize(nV);
			}
		}
		fs.close();
	}
	else cout <<"ERROR";
	return allEdges;
}

void Graph::loadResidualGraph(string fileName)
{
	ifstream fs;
	string in;
	int origin, target, capacity,flow;
	for(int i = 0; i < graph.size(); i++) graph.pop_back();
		fs.open(fileName.c_str());
		if (fs.is_open()){
			while(!fs.eof()){
				fs >> in;
				if(!in.compare("a")){
					fs>>in;origin = atoi (in.c_str());
					fs>>in;target = atoi (in.c_str());
					fs>>in;capacity = atoi (in.c_str());
					flow = 0;
				addEdge(origin,target,capacity,flow, FORWARD_EDGE); // isForward = true 
				addEdge(target,origin,0,flow, BACKWARD_EDGE);
			} else if (!in.compare("p")) {
				string dummy;
				unsigned nV, QtdEdges;
				fs >> dummy >> nV >> QtdEdges;
				graph.resize(nV);
			}
		}
		fs.close();
	}
	return ;
}


void Graph::addEdge(int origin, int target, int capacity, int flow, bool isForward){
	ListAdj ListAdj;
	Vertex vertex(target,capacity,flow,isForward);
	if(origin > (int)graph.size()){
		for(int i = graph.size(); i < origin; i++){
			ListAdj = newVertex(i+1,isForward);
			insertVertex(ListAdj);
		}
	}
	graph[origin-1].push_back(vertex);
}

ListAdj Graph::insertNeighbour(ListAdj ListAdj,int id, int capacity,int flow,bool isForward){
	Vertex v(id,capacity,flow,isForward);
	ListAdj.push_back(v);
	return ListAdj;
}

void Graph::insertVertex(ListAdj ListAdj){
	graph.push_back(ListAdj);
}

ListAdj* Graph::getNeighbourhood(int id){
	return &(graph[id-1]);
}

Vertex Graph::getEdgeFromTo(int idOrigin,int idTarget){
	ListAdj *n = getNeighbourhood(idOrigin);
	Vertex  u(0,0,0,true);
	ListAdj::iterator it;
	for(it = n->begin(); it != n->end(); it++){
		u = (*it);
		if (u.getId() == idTarget){
			return u;
		}
	}
}

Vertex Graph::getBackwardEdgeFromTo(int idOrigin,int idTarget){
	ListAdj *n = getNeighbourhood(idOrigin);
	ListAdj::iterator it;
	for(it = n->begin(); it != n->end(); it++){
		if ((*it).getId() == idTarget && !(*it).getIsForward()){
			return (*it);
		}
	}
}

Vertex Graph::getForwardEdgeFromTo(int idOrigin,int idTarget){
	ListAdj *n = getNeighbourhood(idOrigin);
	ListAdj::iterator it;
	for(it = n->begin(); it != n->end(); it++){
		if ((*it).getId() == idTarget && (*it).getIsForward()){
			return (*it);
		}
	}
}

void Graph::updateEdgeFromTo(int idOrigin,int idTarget,int capacity,int flow,bool isForward){
	ListAdj *n = getNeighbourhood(idOrigin);
	ListAdj::iterator it;
	for(it = n->begin(); it != n->end(); it++){
		if ((*it).getId() == idTarget ){
			(*it).setCapacity(capacity);
			(*it).setFlow(flow);
		}
	}
}

void Graph::print()
{
	vector<ListAdj>::iterator node;
	ListAdj::iterator it;
	int id;
	for(node = graph.begin(); node != graph.end(); node++){
		id = (*node).front().getId();
		for(it = (*node).begin(); it != (*node).end(); it++){
			string a = ((*it).getIsForward())? ("F"): ("B");
			cout << id << " " << (*it).getId() << " " << (*it).getCapacity() << " "<< a << endl;
		}
	}
	cout << "Graph Size" <<graph.size() << endl;
}

void Graph::printNeighbourhood(int idOrigin)
{
	ListAdj *n = getNeighbourhood(idOrigin);
	ListAdj::iterator it;
	for(it = n->begin(); it != n->end(); it++){
		cout << " id:" << (*it).getId() << "  C:" << (*it).getCapacity()<<"  F:" << (*it).getFlow()<<"  Forw:" << (*it).getIsForward() << endl;
	}
}

ListAdj Graph::newVertex(int id, bool isForward){
	Vertex v(id,0,0,isForward);
	ListAdj ListAdj;
	ListAdj.push_back(v);
	return ListAdj;
}

void Graph::updateGraph(vector<int> path, int bottleneck){
	Vertex u(0,0,0,true);
	for (int i = 1; i < path.size(); ++i)
	{
		u = getEdgeFromTo(path[i-1],path[i]);
		int cap = u.getCapacity(); //not used 0 = 0
		int flw = u.getFlow()+bottleneck;
		updateEdgeFromTo(path[i-1],path[i],cap,flw,true);
	}
}

void Graph::updateResidualGraph(vector<int> path, int bottleneck){
	Vertex u(0,0,0,true);
	Vertex v(0,0,0,false);
	for (int i = 1; i < path.size(); ++i)
	{
		u = getForwardEdgeFromTo(path[i-1],path[i]);
		if (u.getIsForward()){
			int foo = u.getCapacity()-bottleneck;
			int bar = u.getFlow();
			updateEdgeFromTo(path[i-1],path[i],foo,bar,true);
		}
		v = getBackwardEdgeFromTo(path[i-1],path[i]);
		if (!v.getIsForward()){
			int foo = v.getCapacity()+ bottleneck;
			int bar = v.getFlow();
			updateEdgeFromTo(path[i-1],path[i],foo,bar,false);
		}
	}
}
void Graph::printPath(Path path){
	cout<<endl;
	cout<<"----------------PrintPath---------------"<<endl;
	cout<<endl;
	for (int i = 0; i < path.size(); ++i)
	{
		cout<<path[i]<<"-->";
	}
	cout<<endl;
}


