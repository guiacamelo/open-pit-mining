
#!/bin/bash
rm batch1/*.ins
rm batch1/*.pbm
rm batch1/*.sol
for i in {2..20}
do
	
	for j in {2..20}
	do
    	./opm --w $j --h $i --ins batch1/I$i-$j.ins
    	./pitMining < batch1/I$i-$j.ins 2> batch1/I$i-$j.sol
		./opm --ins batch1/I$i-$j.ins --sol batch1/I$i-$j.sol --pbm batch1/I$i-$j.pbm 
	done

done

for i in {1..9}
do
	./opm --ins I$i.ins --sol I$i.sol --pbm I$i.pbm 
done