#include <iostream>
#include <list>
#include <vector>
#include <string>
#include "vertex.h"
using namespace std;

typedef list<Vertex> ListAdj;
typedef vector<int> Path;
class Graph {

#define FORWARD_EDGE true
#define BACKWARD_EDGE false

public:
	vector<ListAdj> graph;
	
	Graph();
	


	ListAdj insertNeighbour(ListAdj ListAdj,int id, int distance,int flow,bool isForward);
	ListAdj newVertex(int id,bool isForward);
	void print();
	int graphSize(){
		return graph.size();
	}
	void graphReSize(int nV){
		graph.resize(nV);
		return;
	}

	void addEdge(int origin, int target, int capacity,int flow,bool isForward); 
	void insertVertex(ListAdj ListAdj);
	ListAdj* getNeighbourhood(int id);

	vector<int> loadGraph(string file);
	void loadResidualGraph(string fileName);
	void populateResidualGraph(vector<int> allEdges);
	void loadResidualGraphFromStdin(string fileName);
	void populateResidualGraphFromStdin(vector<int> allEdges);
	
	Vertex getEdgeFromTo(int idOrigin,int idTarget);
	void printNeighbourhood(int idOrigin);
	void updateEdgeFromTo(int idOrigin,int idTarget,int capacity,int flow,bool isForward);
	void updateGraph(Path path, int bottleneck);
	void updateResidualGraph(Path path, int bottleneck);
	void printPath(Path path);
	Vertex getBackwardEdgeFromTo(int idOrigin,int idTarget);
	Vertex getForwardEdgeFromTo(int idOrigin,int idTarget);

};