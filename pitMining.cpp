#include "graph.h"
#include <stdlib.h> 
#include <stdio.h>
#include <fstream>
#include <cassert>
#include <time.h>
#include <iostream>
#include <string> 
#include <algorithm>
#include <queue>
#include <string.h> 

#define MAX_CAPACITY 100000
int T = -1;
int S = 1;

void  loadPitMap(istream& in, vector<int>& matrix, int& width, int& height) {
	string line = "", dummy;
	string buffer;
	vector<int> allEdges(0);
	in >> buffer;
	width = atoi (buffer.c_str());
	in >> buffer;
	height = atoi (buffer.c_str());
	int c;
	matrix.push_back(0);
	matrix.push_back(0); // Information start in index 2, the index 1 is S
	for (int j = 1; j <= height; ++j)
	{
		for (int i = 1; i <= width; ++i)
		{
			in>>buffer;
			c = atoi (buffer.c_str());
			matrix.push_back(c);
		}
	}
	return;

}

void populateGraph(Graph& graph, Graph& rGraph, vector<int> matrix,int width,int height) {
	int blockIndex;
	for (int j = 1; j <= height; ++j)
	{
		for (int i = 1; i <= width; ++i)
		{
			blockIndex = i + j*width - width+1;
			if(matrix[blockIndex] == 0){ /*cout << "ERROR, Block cost is 0"<<endl */;}
			if ((matrix[blockIndex] > 0)){  // create edge from S to current block node
				graph.addEdge(S,blockIndex,matrix[blockIndex],0, FORWARD_EDGE); // isForward = true 
				rGraph.addEdge(S,blockIndex,matrix[blockIndex],0, FORWARD_EDGE); // isForward = true 
				rGraph.addEdge(blockIndex,S,0,0, BACKWARD_EDGE);
			}else {	// create edge from S to current block node				
				graph.addEdge(blockIndex,T,abs(matrix[blockIndex]),0, FORWARD_EDGE); // isForward = true 
				rGraph.addEdge(blockIndex,T,abs(matrix[blockIndex]),0, FORWARD_EDGE); // isForward = true 
				rGraph.addEdge(T,blockIndex,0,0, BACKWARD_EDGE);
			}
		}
	}
}

void createInfiniteEdges(Graph& graph, Graph& rGraph, vector<int> matrix,int width,int height) {
	int blockIndex;
	int blockUpIndex;
	int blockUpRIndex;
	int blockUpLIndex;

	bool isLeftBlock = false;
	bool isRightBlock = false;

	for (int j = 2; j <= height; ++j) // only add dependece edges in blocks below the surfice 
	{
		isRightBlock = false;
		isLeftBlock = true;
		for (int i = 1; i <= width; ++i)
		{


			if (i == 1){ // identifies that block is close to the left border in the matrix
				isLeftBlock = true;
			} else{
				isLeftBlock = false;
			} 
			if (i == width){ // identifies that block is close to the right border in the matrix
				isRightBlock = true;
			}else{
				isRightBlock = false;
			}

			blockIndex = i + j*width - width +1;
			blockUpIndex  = blockIndex - width;
			blockUpRIndex = blockIndex - width + 1;
			blockUpLIndex = blockIndex - width - 1;
			// Create edge with infinite capacity to upper block
			graph.addEdge(blockIndex,blockUpIndex,MAX_CAPACITY,0, FORWARD_EDGE); // isForward = true 
			rGraph.addEdge(blockIndex,blockUpIndex,MAX_CAPACITY,0, FORWARD_EDGE); // isForward = true 
			rGraph.addEdge(blockUpIndex,blockIndex,0,0, BACKWARD_EDGE);
			
			if(!isLeftBlock ){
				// Create edge with infinite capacity to upper left block
				graph.addEdge(blockIndex,blockUpLIndex,MAX_CAPACITY,0, FORWARD_EDGE); // isForward = true 
				rGraph.addEdge(blockIndex,blockUpLIndex,MAX_CAPACITY,0, FORWARD_EDGE); // isForward = true 
				rGraph.addEdge(blockUpLIndex,blockIndex,0,0, BACKWARD_EDGE);
			}
			if(!isRightBlock ){
				// Create edge with infinite capacity to upper right block
				graph.addEdge(blockIndex,blockUpRIndex,MAX_CAPACITY,0, FORWARD_EDGE); // isForward = true 
				rGraph.addEdge(blockIndex,blockUpRIndex,MAX_CAPACITY,0, FORWARD_EDGE); // isForward = true 
				rGraph.addEdge(blockUpRIndex,blockIndex,0,0, BACKWARD_EDGE);
			}

		}
	}
	return ;
}

vector<int> getReachableNodes(Graph& rGraph, vector<int> visited, int qtdNodes){
	vector<int> reachableNodes(0);
	vector<bool> flagVisited(qtdNodes+2);
	for (int i = 1; i < qtdNodes+2; ++i)
	{
		flagVisited[i] = false;
	}

	ListAdj *n;
	ListAdj::iterator it;
	queue<int> q = queue<int>();
	flagVisited[S] = true;
    q.push(S); //enqueue
    while (!q.empty()){
    	int node = q.front();
    	q.pop(); //dequeue
    	n=rGraph.getNeighbourhood(node);
    	for(it = n->begin(); it != n->end(); it++){
    		if(!flagVisited[(*it).getId()] && ((*it).getCapacity() > 0) && (*it).getIsForward()){
    			reachableNodes.push_back((*it).getId());
    			flagVisited[(*it).getId()] = true;
    			if ((*it).getId() == T){
    				//cout<<" ERRO NÂO DEVERIA ALCANÇAR T"<<endl;
    			}else{
    				q.push((*it).getId());
    			}
    		}
		}
    }
    return reachableNodes;
}

Path findNextPathEK(Graph& rGraph, vector<int> visited, int qtdNodes){
	vector<int> pred(qtdNodes+2);
	vector<bool> flagVisited(qtdNodes+2);
	for (int i = 1; i < qtdNodes+2; ++i)
	{
		pred[i] = -1;
		flagVisited[i] = false;
	}
	ListAdj *n;
	ListAdj::iterator it;
	queue<int> q = queue<int>();
	flagVisited[S] = true;
    q.push(S); //enqueue
    while (!q.empty()){
    	int node = q.front();
    	q.pop(); //dequeue
    	n=rGraph.getNeighbourhood(node);
    	for(it = n->begin(); it != n->end(); it++){
    		if(!flagVisited[(*it).getId()] && (*it).getCapacity() > 0){
    			pred[(*it).getId()] = node;
    			flagVisited[(*it).getId()] = true;
    			if ((*it).getId() == T){
    				Path pathT; 
    				int foo = (*it).getId();
    				while (foo > 0) {
    					pathT.push_back(foo);
    					foo = pred[foo];
    				}
    				reverse(pathT.begin(), pathT.end());
    				return pathT;
    			}
    			else{
    				q.push((*it).getId());
    			}

    		}
    	}
    }
    return  vector<int>();

}

//Check if node is in vector <visited> 
bool contains(int value, std::vector<int> vector) 
{
	for(int i = 0; i < vector.size(); i++)
	{
		if(vector[i] == value)
		{
			return true;
		}
	}
	return false;
}

int findBottleneck(Graph& rGraph,Path path){
	int bottleneck=100000;
	Vertex u(0,0,0,true);
	for (int i = 1; i < path.size(); ++i)
	{
		u = rGraph.getEdgeFromTo(path[i-1],path[i]);
		if(u.getCapacity()<bottleneck){
			bottleneck = u.getCapacity();
		}
	}
	return bottleneck;
}
vector<Graph> augment(Graph& graph,Graph& rGraph,Path path){
	int bottleneck=findBottleneck(rGraph,path);
	int capacityForward,capacityBackward,flow;
	Vertex u(0,0,0,true);
	Vertex v(0,0,0,true);
	Vertex w(0,0,0,false);

	for (int i = 1; i < path.size(); ++i)
	{
		v = rGraph.getEdgeFromTo(path[i-1],path[i]);
		if(v.getIsForward()){
			u = graph.getEdgeFromTo(path[i-1],path[i]);
			w = rGraph.getEdgeFromTo(path[i],path[i-1]);
			flow = u.getFlow() + bottleneck;
			capacityForward =  v.getCapacity() - bottleneck;
			capacityBackward = w.getCapacity() + bottleneck;
			graph.updateEdgeFromTo(path[i-1],path[i],u.getCapacity(),flow,u.getIsForward());
			rGraph.updateEdgeFromTo(path[i-1],path[i],capacityForward,v.getFlow(),v.getIsForward());
			rGraph.updateEdgeFromTo(path[i],path[i-1],capacityBackward,w.getFlow(),w.getIsForward());
			w = rGraph.getEdgeFromTo(path[i],path[i-1]);
		}else{
			u = graph.getEdgeFromTo(path[i],path[i-1]);
			w = rGraph.getEdgeFromTo(path[i],path[i-1]);
			flow = u.getFlow() - bottleneck;
			capacityForward =  w.getCapacity() + bottleneck;
			capacityBackward = v.getCapacity() - bottleneck;
			graph.updateEdgeFromTo(path[i],path[i-1],u.getCapacity(),flow,u.getIsForward());
			rGraph.updateEdgeFromTo(path[i],path[i-1],capacityForward,w.getFlow(),w.getIsForward());
			rGraph.updateEdgeFromTo(path[i-1],path[i],capacityBackward,v.getFlow(),v.getIsForward());
		}

	}
	vector<Graph> graphs(0);
	graphs.push_back(graph);
	graphs.push_back(rGraph);
	return graphs;
}

void printMatrix(vector<int> matrix,int width, int height){
	cerr<< width <<" "<<  height << endl;
	for (int j = 1; j <= height; ++j)
	{
		for (int i = 1; i <= width; ++i)
		{	
			cerr<<matrix[i + j*width - width-1]<<" ";
		}
		cerr<<endl;
	}
}

int main(int argc, char *argv[])
{
	
	clock_t tStart = clock();
	vector<int> matrix(0);
	int width,height;
	loadPitMap(cin,matrix,width,height);

	Graph graph;  
	Graph rGraph; 
	//criar grafo
	
	int blockIndex;	
	int	blockValeu;
	int totalFlow;
	Path path(0);
	vector<int> reachableNodes(0);
	T=matrix.size(); 
	// criar arestestas de s até todos positivos, e  criar arestes de todos os negativos até t
	populateGraph(graph,rGraph,matrix,width,height);
	
	// criar arestas infinitas aos vizinhos
	createInfiniteEdges(graph,rGraph,matrix,width,height);
	// executar caminho máximo
	int bottleneck = 100000;
	vector<int> visited(0);
	visited.push_back(S);
		
	while(!findNextPathEK(rGraph,visited,graph.graphSize()).empty()){
		path.clear();
		path.push_back(S);
		visited.clear();
		visited.push_back(S);
		path= findNextPathEK(rGraph,visited,graph.graphSize());
		bottleneck=findBottleneck(rGraph,path);
		vector<Graph> graphs(0); 
		graphs = augment(graph,rGraph,path);
		graph = graphs[0];
		rGraph = graphs[1];
		totalFlow+=bottleneck;
		bottleneck = 100000;
		visited.clear();
		visited.push_back(S);	
	}

	// verificar a quais nós se consegue chegar, esses nós são explorados 
	reachableNodes = getReachableNodes(rGraph,visited,rGraph.graphSize());
	vector<int> resultMatrix(0);
	for (int j = 1; j <= height; ++j)
	{
		for (int i = 1; i <= width; ++i)
		{
			resultMatrix.push_back(0);
		}
	}	
	int totResult=0;
	
	for (int i = 0; i < reachableNodes.size(); ++i)
	{
		resultMatrix[reachableNodes[i]-2]=1;
		totResult = totResult +  matrix[reachableNodes[i]];
	}	
	cout<<totResult<<endl;
	printMatrix(resultMatrix, width,height);

	return 0;
}
